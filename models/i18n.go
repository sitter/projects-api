// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package models

import (
	"encoding/json"
)

// I18nInputData is an auxiliary struct used to unmarshal the input data from
// disk which uses snake_case in its json representation. This struct should
// not be used directly, it is used by UnmarshalJSON whilst working with
// a I18nData. I18nData's UnmarshalJSON should be kept in sync with
// new fields here!
type i18nInputData struct {
	Stable       string `json:"stable"`
	StableKF5    string `json:"stable_kf5"`
	StableLTSKF5 string `json:"stable_lts_kf5"`
	Trunk        string `json:"trunk"`
	TrunkKF5     string `json:"trunk_kf5"`
}

// I18nData represents I18n relevant data.
type I18nData struct {
	// TODO v2 mark everything omitempty
	// TODO v2 consider nesting by type {kf5:{trunk:master},kde4:{},kf6:{},...}
	Stable       string `json:"stable"`
	StableKF5    string `json:"stableKF5"`
	StableLTSKF5 string `json:"stableLTSKF5,omitempty"`
	Trunk        string `json:"trunk"`
	TrunkKF5     string `json:"trunkKF5"`

	// Custom fields not found in input data. Calculated by us.
	Component string `json:"component"`
}

// Merge merges another i18nData into this i18nData. The other's values always
// override unless they are empty.
func (i *I18nData) Merge(o I18nData) {
	if o.Stable != "" {
		i.Stable = o.Stable
	}
	if o.StableKF5 != "" {
		i.StableKF5 = o.StableKF5
	}
	if o.StableLTSKF5 != "" {
		i.StableLTSKF5 = o.StableLTSKF5
	}
	if o.Trunk != "" {
		i.Trunk = o.Trunk
	}
	if o.TrunkKF5 != "" {
		i.TrunkKF5 = o.TrunkKF5
	}
}

// Infer infers additional properties of I18nData from its Project.
func (i *I18nData) Infer(project *Project) {
	// With the move to gitlab we have a new identifier that is used as
	// k18n component (and is now per-repo). This replaces the shoddy
	// inference from before.
	i.Component = project.Identifier
}

// UnmarshalJSON unmarshals the input snake_case attributes into our struct
// which will marshal to CamelCase upon output via encoding tags.
func (i *I18nData) UnmarshalJSON(data []byte) error {
	// This would be oh-so unnecessary if the de/encoder simply allowed specifying
	// an arbitrary tag instead of json so one could use two distinct tags for
	// input and output -.-
	m := i18nInputData{}
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}
	i.Stable = m.Stable
	i.StableKF5 = m.StableKF5
	i.StableLTSKF5 = m.StableLTSKF5
	i.Trunk = m.Trunk
	i.TrunkKF5 = m.TrunkKF5
	return nil
}
