// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

package models

import (
	"net/url"
)

// Project is the core model of a project entity.
type Project struct {
	// TODO v2 omitempty
	I18n I18nData `yaml:"i18n" json:"i18n"`

	// With the migration to invent projectpath became useless. It's the legacy
	// path for awkward reasons. Instead publish the real on disk path and
	// expose the legacy path under a different name.
	PathOnDisk string `yaml:"-" json:"path"`
	// TODO v2 throw away hopefully (depends on metadata getting better replacement
	// Used by:
	// - kde-org-applications-extractor (needed to map to ci-tooling data)
	PathInMetadata string `yaml:"projectpath" json:"__do_not_use-legacy-projectpath,omitempty"`

	Repo string `yaml:"repopath" json:"repo"`
	// TODO v2 throw away hopefully (depends on metadata getting better replacement)
	Active     bool   `yaml:"repoactive" json:"-"`          // do not marshal to json, we presently have no use case for it
	Identifier string `yaml:"identifier" json:"identifier"` // also marshal'd via I18nData

	Bugzilla BugzillaData `yaml:"bugzilla" json:"-"` // Currently there is no usecase of exporting the bugzilla data.

	// Custom fields not found in input data. Calculated by us.
	BugDatabase string `json:"bug_database"`
	BugSubmit string `json:"bug_submit"`
}

// Infer infers additional properties of BugzillaData into its Project.
// This function needs to be called explicitly to fill BugSubmit and BugDatabase.
func (bugzilla *BugzillaData) Infer(project *Project) {
	// BugSubmit and BugDatabase urls are calculated by the BugzillaData.

	params := url.Values{}
	if bugzilla.Product != "" {
		params.Add("product", bugzilla.Product)
	}
	if bugzilla.Component != "" {
		params.Add("component", bugzilla.Component)
	}
	if len(params) > 0 {
		project.BugSubmit = "https://bugs.kde.org/enter_bug.cgi?" + params.Encode()
		params.Add("resolution", "---")
		project.BugDatabase = "https://bugs.kde.org/buglist.cgi?" + params.Encode()
	}
}
