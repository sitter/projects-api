// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package models

// ProjectFilter is used to filter projects on find() calls.
// The type(names) in this struct must be chosen so that an uninitialized
// filter results in match-all behavior!
type ProjectFilter struct {
	ID           string `form:"id"`       // DEPRECATED! replaced by basename, don't use
	Basename     string `form:"basename"` // basename of the project
	RepoPath     string `form:"repo"`     // complete repo path
	ActiveOnly   bool   `form:"active"`   // whether to select active projects
	InactiveOnly bool   `form:"inactive"` // whether to select inactive projects
	AnyI18n      bool   `form:"any_i18n"` // select only ones with any i18n branch
}
