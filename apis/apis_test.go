// SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package apis

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var router *gin.Engine

func init() {
	router = gin.Default()
}

type apiTestCase struct {
	tag      string
	method   string
	url      string
	body     string
	status   int
	response string
}

func testAPI(method, URL, body string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, URL, bytes.NewBufferString(body))
	req.Header.Set("Content-Type", "application/json")
	res := httptest.NewRecorder()
	router.ServeHTTP(res, req)
	return res
}

func runAPITests(t *testing.T, tests []apiTestCase) {
	for _, test := range tests {
		t.Run(test.tag, func(t *testing.T) {
			res := testAPI(test.method, test.url, test.body)
			assert.Equal(t, test.status, res.Code, test.tag)
			if test.response != "" {
				assert.JSONEq(t, test.response, res.Body.String(), test.tag)
			}
		})
	}
}
