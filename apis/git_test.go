// SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package apis

import (
	"net/http"
	"testing"
	"time"
)

// Test Double
type GitService struct {
	age time.Duration
}

func NewGitService() *GitService {
	age, _ := time.ParseDuration("5m")
	return &GitService{age}
}

func (s *GitService) UpdateClone() string {
	s.age = 0
	return "UPDATED"
}

func (s *GitService) Age() time.Duration {
	return s.age
}

func init() {
	v1 := router.Group("/v1")
	{
		ServeGitResource(v1, NewGitService())
	}
}

func TestGit(t *testing.T) {
	runAPITests(t, []apiTestCase{
		{"t1 - poll", "GET", "/v1/poll", "", http.StatusOK, `"UPDATED"`},
		{"t2 - poll", "GET", "/v1/poll", "", http.StatusTooManyRequests, `"Not updating. Last update was 0s ago."`},
	})
}
