// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package services

import (
	"testing"
	"time"

	"invent.kde.org/sysadmin/projects-api.git/models"

	"github.com/stretchr/testify/assert"
)

type GitDAO struct {
}

func NewGitDAO() *GitDAO {
	return &GitDAO{}
}

func (d *GitDAO) Age() time.Duration {
	return 0
}

func (d *GitDAO) Get(path string) (*models.Project, error) {
	if path == "calligra/krita" {
		return &models.Project{
			Repo:       "kritarepo",
			Identifier: "kritaid",
			PathOnDisk: path,
		}, nil
	}
	if path == "calligra/krita-extensions/krita-analogies" {
		return &models.Project{
			Repo:       "krita-analogiesrepo",
			Identifier: "krita-analogiesid",
			Active:     true,
			PathOnDisk: path,
		}, nil
	}
	if path == "frameworks/solid" {
		return &models.Project{
			Repo:       "solidrepo",
			Identifier: "solidid",
			PathOnDisk: path,
		}, nil
	}
	if path == "calligra" {
		return nil, nil
	}
	panic("unknown path requested " + path)
}

func (d *GitDAO) List() []string {
	return []string{"calligra", "calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}
}

func (d *GitDAO) UpdateClone() string {
	return ""
}

func TestProjectFind(t *testing.T) {
	s := NewProjectService(NewGitDAO())

	t.Run("no filter", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}, ret)
	})

	t.Run("filter by id", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{Basename: "krita"})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})

	t.Run("filter by repo", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{RepoPath: "kritarepo"})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})

	t.Run("filter by active", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{ActiveOnly: true})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita-extensions/krita-analogies"}, ret)
	})
}

func TestProjectList(t *testing.T) {
	s := NewProjectService(NewGitDAO())

	t.Run("root", func(t *testing.T) {
		ret, err := s.List("")
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}, ret)
	})

	t.Run("component", func(t *testing.T) {
		ret, err := s.List("calligra")
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies"}, ret)
	})

	t.Run("project", func(t *testing.T) {
		ret, err := s.List("calligra/krita")
		assert.NoError(t, err)
		// This must not include krita-extensions as it is not a child, it happens
		// to have the same stringy prefix though.
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})
}
func TestProjectIdentifiers(t *testing.T) {
	s := NewProjectService(NewGitDAO())

	t.Run("get-by-id", func(t *testing.T) {
		ret, err := s.GetByIdentifier("kritaid")
		assert.NoError(t, err)
		assert.Equal(t, "kritaid", ret.Identifier)
		assert.Equal(t, "kritarepo", ret.Repo)
	})

	t.Run("list", func(t *testing.T) {
		filter := &models.ProjectFilter{}
		ret, err := s.Identifiers(filter)
		assert.NoError(t, err)
		assert.Equal(t, []string{"kritaid", "krita-analogiesid", "solidid"}, ret)
	})

	t.Run("filter-only-active", func(t *testing.T) {
		filter := &models.ProjectFilter{ActiveOnly: true}
		ret, err := s.Identifiers(filter)
		assert.NoError(t, err)
		assert.Equal(t, []string{"krita-analogiesid"}, ret)
	})
}
