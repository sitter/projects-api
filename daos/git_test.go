// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

package daos

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Shared dao so tests don't clone over and over again (implies that
// TestGitUpdateClone is working ;))
var dao = newGitDAOInternal(false)

func TestGitUpdateClone(t *testing.T) {
	tmpdir, _ := ioutil.TempDir("", "")
	twd, _ := os.Getwd()
	twd = twd + "/"
	os.Chdir(tmpdir)
	defer func() {
		os.Chdir(twd)
		os.RemoveAll(tmpdir)
	}()
	// DO NOT MUTATE TWD BELOW!

	// No clone
	pwd, _ := os.Getwd()
	fmt.Println(pwd)
	_, err := os.Stat("repo-metadata")
	assert.Error(t, err)

	cloneDAO := newGitDAOInternal(false)
	cloneDAO.UpdateClone()

	// Clone now
	pwd, _ = os.Getwd()
	fmt.Println(pwd)
	_, err = os.Stat("repo-metadata")
	assert.NoError(t, err)
}

func TestGitGet(t *testing.T) {
	project, err := dao.Get("frameworks/solid")

	assert.NoError(t, err)
	assert.NotNil(t, project)
	assert.Equal(t, "frameworks/solid", project.PathInMetadata)
	assert.Equal(t, "frameworks/solid", project.PathOnDisk)
	assert.Equal(t, "frameworks/solid", project.Repo)
	assert.Equal(t, "https://bugs.kde.org/enter_bug.cgi?product=frameworks-solid", project.BugSubmit)
	assert.Equal(t, "https://bugs.kde.org/buglist.cgi?product=frameworks-solid&resolution=---", project.BugDatabase)
	assert.Equal(t, "master", project.I18n.TrunkKF5)
	assert.Equal(t, "none", project.I18n.StableKF5)
	assert.Equal(t, true, project.Active)
}

func TestGitList(t *testing.T) {
	// We work on live data, so we can't assert much here. Other than list not
	// being empty.
	assert.NotEmpty(t, dao.List())
}

func TestMain(m *testing.M) {
	tmpdir, _ := ioutil.TempDir("", "")
	twd, _ := os.Getwd()
	twd = twd + "/"
	os.Chdir(tmpdir)
	defer func() {
		os.Chdir(twd)
		os.RemoveAll(tmpdir)
	}()
	// DO NOT MUTATE TWD BELOW!

	dao.UpdateClone()

	os.Exit(m.Run())
}
