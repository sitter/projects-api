# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

clean:
	rm -rf doc

node_modules/.bin/apidoc:
	npm install apidoc

deploy: test
	ruby deploy.rb

doc: node_modules/.bin/apidoc
	node_modules/.bin/apidoc \
		--debug \
		-e node_modules \
		-e vendor \
		-e doc \
		-o doc

projects-kde-org-api:
	go build -o projects-kde-org-api -v

run: projects-kde-org-api
	/usr/bin/systemd-socket-activate -l 0.0.0.0:8080 ./projects-kde-org-api

test:
	go test -v -coverpkg=./... -coverprofile=coverage.cov ./...

coverage.cov: test

coverage: coverage.cov
	# this target is intentionally phony so it's always dirty
	go tool cover -func coverage.cov
	go tool cover -html=coverage.cov -o coverage.html

.PHONY: deploy run tets projects-kde-org-api doc coverage # doc and the bin are phony because they need to be updated!
